document.addEventListener("DOMContentLoaded", LateLoad)

class projekt {
    constructor(projectName, name = projectName, osztaly = 9, icon= "../bimg.png", desc="Egy iskolai projekt", html = "index.html", ev="0000", honap="00", nap="00", tags=[]) {
        this.projectName = projectName;
        this.name = name;
        this.osztaly = osztaly;
        this.icon = icon;
        this.desc = desc;
        this.html = html;
        this.tags = tags;
        if (honap < 10) honap = "0" + honap;
        if (nap < 10) nap = "0" + nap;
        this.datum = [ev, honap, nap].join("-");
    }

    getDatum() {
        return this.datum.split("-")
    }
}

const webProjects = [
    new projekt("1_iskola", "Iskola", 9, "pngs/iskola.png", "Az első weboldal"),
    new projekt("2_autok", "Autók", 9, "pngs/autok.png", "Bevezetés a linkekbe"),
    new projekt("3_sakk", "Sakk", 9, "pngs/sakk.png", "Stílusok használata"),
    new projekt("4_fozelek", "Főzelék", 9, "pngs/fozelek.png", "Gyakorlás"),
    new projekt("5_PAPRIKA", "Paprika", 9, "pngs/paprika.png", "Gyakorlás"),
    new projekt("6_getcoding", "Get coding", 9, "pngs/getcoding.png", "Szebb stílusok"),
    new projekt("7_osember", "Ősember", 9, "pngs/osember.png", "Minimalista és szép"),
    new projekt("8_halma", "Halma", 9, "pngs/halma.png", "~fejltettebb stílusok"),
    new projekt("9_doga", "Metanol - Dolgozat", 9, "pngs/doga.png", "Az első dolgozat", "metanol.html"),
    new projekt("10_szg_jatekok", "Játékok", 9, "pngs/szg_jatekok.png", "Befejezetlen"),
    new projekt("11_lepke", "Lepke", 9, "pngs/lepke.png", "Gyakorlás"),
    new projekt("10_1_haj", "Haj", 10, "pngs/haj.png", "10. osztály első weboldala", "haj.html", 2025, 1, 22, ["első"]),
    new projekt("10_2_kerekparos_kresz", "Kerékpáros KRESZ", 10, "pngs/kerekparos_kresz.png", "1. Ágazati felkészítő", "index.html", 2025, 2, 5, ["ágazati alapvizsga"]),
    new projekt("10_3_kapor", "Kapor", 10, "pngs/kapor.png", "2. Ágazati felkészítő", "kapor.html", 2025, 2, 19, ["ágazati alapvizsga"]),
    new projekt("10_4_telefon", "Telefon", 10, "pngs/telefon.png", "2025.02.19-én feladott házi", "index.html", 2025, 2, 25, ["házi feladat", "ágazati alapvizsga"]),
    new projekt("10_5_ipv4_cimek", "A hálózati címfordítás", 10, "pngs/ipv4cimek_szamolasa.png", "2025.02.26-én feladott házi", "index.html", 2025, 2, 28, ["házi feladat", "ágazati alapvizsga"]),
    new projekt("10_6_orchideak", "Orchideák", 10, "pngs/orchideak.png", "2025.03.05-én megoldott órai munka", "index.html", 2025, 3, 5, ["órai munka", "gyakorlás"])
];

const allProjects = [...webProjects]

let currentProjects = allProjects;

function AddProject(p) {
    const template = document.querySelector("[tartalom]")

    const resoult = template.content.cloneNode(true);
    if (resoult) {
        resoult.querySelector("a").href = `projects/${p.projectName}/${p.html}`
        resoult.querySelector(".cim").textContent = p.name
        resoult.querySelector(".desc").innerHTML = p.desc
                    .replaceAll("len", "<span style='color:red'>len</span>")
        resoult.querySelector("img").src = p.icon
    
        document.querySelector("[webHolder]").appendChild(resoult);
    } else {
        console.error("Resoult returned null")
    }
}

function AddProjects(projects=[]) {
    document.querySelector("[webHolder]").innerHTML = ""
    projects.forEach(p => { AddProject(p); })
}

function UpdateOsztaly() {
    let o = Number.parseInt(document.querySelector("#osztalySelect").value);
    localStorage.setItem("o", o)
    let pr = [];
    if (o == 0)
        pr = [...webProjects]
    else
        pr = webProjects.filter(p => p.osztaly == o)
    currentProjects = pr;
    AddProjects(currentProjects)
}

function LateLoad() {
    document.querySelector("#osztalySelect").value = localStorage.getItem("o") || "0"
    document.querySelector("#sb").value = localStorage.getItem("sp_sb") || ""
    UpdateSB();
    Update();
}

function UpdateSB() {
    let text = document.querySelector("#sb").value;
    localStorage.setItem("sp_sb", text)
    UpdateOsztaly();
    currentProjects = currentProjects.filter(p => 
        Latinify(p.name.toLowerCase()).includes(Latinify(text.toLowerCase())) ||
        Latinify(p.projectName.toLowerCase()).includes(Latinify(text.toLowerCase())) ||
        p.tags.some(h => Latinify(h).toLowerCase().includes(Latinify(text.toLowerCase())))
    )
    AddProjects(currentProjects)
}

function Latinify(str) {
    return str
    .replaceAll("á", "a").replaceAll("Á", "A")
    .replaceAll("é", "e").replaceAll("É", "E")
    .replaceAll("ó", "o").replaceAll("Ó", "O")
    .replaceAll("ö", "o").replaceAll("Ö", "O")
    .replaceAll("ő", "o").replaceAll("Ő", "O")
    .replaceAll("ú", "u").replaceAll("Ú", "U")
    .replaceAll("ü", "u").replaceAll("Ü", "U")
    .replaceAll("ű", "u").replaceAll("Ű", "U")
    .replaceAll("í", "i").replaceAll("Í", "I")
    .replaceAll(" ", "")
}

function Update() {
    document.querySelector("[webHolderNope]").style.display = document.querySelector("[webHolder]").innerHTML == "" ? "block" : "none";

    requestAnimationFrame(Update);
}