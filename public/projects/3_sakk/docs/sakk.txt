forrás:
https://chess.hu
https://www.polgarjudit.hu/

Az első világbajnokságon, 1985-ben Luzernben volt. Magyarország a 9 világbajnokságból 4 alkalommal játszott. A férfiválogatott egy ezüstérmet szerzett 1985 Luzernben.

A csapat tagjai: Portisch Lajos, Ribli Zoltán, Sax Gyula, Pintér József, Adorján András, Faragó Iván, Csom István, Grószpéter Attila

Az első Magyar Sakkszövetséget 1911-ben alakították meg. Sajnos, 1913-ban megszűnt. 1921-ben tudták ismét megalakítani a Magyar Sakkszövetséget, amely aztán 1944-ig folyamatosan és sikeresen működött. Az 1921-ben újra megalakított Sakkszövetség jogutódja a jelenlegi szervezet. 

A magyaroknak a sakkjáték ismeretére utaló legkorábbi tárgyi emléke a mai Ukrajna területén, Csernigov közelében, egy IX-X. századi temetőben, egy magyar személyre utaló sírban talált nyolc darab üveg sakkfigura. 

Maróczy Géza 1895-1908 közt világversenyek győztese, holtversenyes első helyezettje, vagy az élcsoport tagja volt. A kor legjobbjainak szűk köréhez tartozott. Az I. világháború után, túl 50. életévén még ért el kiváló eredményeket, olimpiai csapatainknak éltáblása, jeles sakkíró is volt. 

Képfeliratok:
Jose Raul Capablanca, Maróczy Géza
Portisch Lajos
Lékó Péter
1993. Budapest: Borisz Szpasszkij, Polgár Judit