Lada
A Lada az orosz AvtoVAZ járműgyártó vállalat gépkocsimárkája. A szovjet időszakban ezt a márkanevet csak az exportra szánt járműveknél használták, ugyanazokat a modelleket a belső szovjet piacon Zsiguli márkanéven forgalmazták, de az autók a keleti blokkbeli szocialista országokba is cirill betűs Zsiguli felirattal érkeztek a forgalmazás első éveiben.

Trabant
A Trabant egy keletnémet személygépkocsitípus, amelyet a német VEB Sachsenring gyártott. Az első Trabant 1957. november 7-én gurult le a futószalagról, a legelterjedtebb autó volt Kelet-Németországban, és a többi szocialista országba is exportáltak belőle. Neve – jelentése útitárs, követő, csatlós, darabont – az akkoriban fellőtt első szovjet műhold, a Szputnyik–1 német megfelelőjéből származik, mintegy tiszteletből.

Moszkvics
A Moszkvics egy Moszkvában gyártott szovjet autómárka volt. 1947 és 1968 között a Moszkovszkij Zavod Malolitrazsnih Avtomobilej (MZMA), 1968-tól pedig a Lenini Komszomol Autógyár (AZLK – Avtomobilnij Zavod imenyi Lenyinszkovo Komszomola) állította elő. 

Wartburg
A Wartburg egy háromhengeres, kétütemű autó az NDK-ból. A nevét a gyárnak otthont adó Eisenach városban található, az UNESCO világörökség részét képező váráról kapta. 

Forrás: https://hu.wikipedia.org